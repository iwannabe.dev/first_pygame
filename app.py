import pygame


pygame.init()
pygame.display.set_caption("First Pygame!")

SCREEN_WIDTH, SCREEN_HEIGHT = 360, 600
SCREEN = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
FPS = 60

BACKGROUND_IMAGE = pygame.image.load("assets/background.jpg")
SPACESHIP_IMAGE = pygame.image.load("assets/spaceship.png")
BULLET_IMAGE = pygame.image.load("assets/bullet.png")
ENEMY_1_IMAGE = pygame.image.load("assets/enemy1.png")
ENEMY_2_IMAGE = pygame.image.load("assets/enemy2.png")
ENEMY_3_IMAGE = pygame.image.load("assets/enemy3.png")
ENEMY_4_IMAGE = pygame.image.load("assets/enemy4.png")
ENEMY_5_IMAGE = pygame.image.load("assets/enemy5.png")
ENEMY_6_IMAGE = pygame.image.load("assets/enemy6.png")
ENEMY_7_IMAGE = pygame.image.load("assets/enemy7.png")
ENEMY_8_IMAGE = pygame.image.load("assets/enemy8.png")
ENEMY_9_IMAGE = pygame.image.load("assets/enemy9.png")
ENEMY_10_IMAGE = pygame.image.load("assets/enemy10.png")

SPACESHIP_VELOCITY = 4
BULLET_VELOCITY = 10
MAX_BULLETS = 2

WHITE = (255, 255, 255)
TEXT_FONT = pygame.font.SysFont('comicsans', 30)
MESSAGE_FONT = pygame.font.SysFont('comicsans', 60)

ENEMY_HIT = pygame.USEREVENT
ENEMIES = [
    {
        'image': ENEMY_1_IMAGE,
        'level': 1,
        'health': 1,
        'velocity': 3,
        'irregular_moves': False
    },
    {
        'image': ENEMY_2_IMAGE,
        'level': 2,
        'health': 1,
        'velocity': 5,
        'irregular_moves': False
    },
    {
        'image': ENEMY_3_IMAGE,
        'level': 3,
        'health': 2,
        'velocity': 4,
        'irregular_moves': False
    },
    {
        'image': ENEMY_4_IMAGE,
        'level': 4,
        'health': 2,
        'velocity': 6,
        'irregular_moves': False
    },
    {
        'image': ENEMY_5_IMAGE,
        'level': 5,
        'health': 1,
        'velocity': 6,
        'irregular_moves': True
    },
    {
        'image': ENEMY_6_IMAGE,
        'level': 6,
        'health': 2,
        'velocity': 8,
        'irregular_moves': True
    },
    {
        'image': ENEMY_7_IMAGE,
        'level': 6,
        'health': 2,
        'velocity': 8,
        'irregular_moves': True
    },
    {
        'image': ENEMY_8_IMAGE,
        'level': 6,
        'health': 2,
        'velocity': 8,
        'irregular_moves': True
    },
    {
        'image': ENEMY_9_IMAGE,
        'level': 6,
        'health': 2,
        'velocity': 8,
        'irregular_moves': True
    },
    {
        'image': ENEMY_10_IMAGE,
        'level': 6,
        'health': 2,
        'velocity': 8,
        'irregular_moves': True
    },
]


def draw_window(spaceship, enemy, bullets, score, level):
    enemy_image = assign_enemy_image(level)

    SCREEN.blit(BACKGROUND_IMAGE, [0, 0])
    SCREEN.blit(enemy_image, [enemy.x, enemy.y])
    SCREEN.blit(SPACESHIP_IMAGE, [spaceship.x, spaceship.y])

    score_text = TEXT_FONT.render("Score: " + str(score), 1, WHITE)
    SCREEN.blit(score_text, [5, 5])

    level_text = TEXT_FONT.render("Level: " + str(level), 1, WHITE)
    SCREEN.blit(level_text, [SCREEN_WIDTH - level_text.get_width() - 5, 5])

    for bullet in bullets:
        SCREEN.blit(BULLET_IMAGE, [bullet.x, bullet.y])

    pygame.display.update()


def enemy_movement_handler(move_direction, enemy, level):
    if move_direction == "right":
        enemy.x += ENEMIES[level - 1]['velocity']
        if enemy.x >= SCREEN_WIDTH - enemy.width:
            move_direction = "left"
    else:
        enemy.x -= ENEMIES[level - 1]['velocity']
        if enemy.x <= 0:
            move_direction = "right"

    return move_direction


def player_movement_handler(keys_pressed, spaceship):
    if (keys_pressed[pygame.K_LEFT] == True) and (spaceship.x - SPACESHIP_VELOCITY >= 0):
        spaceship.x -= SPACESHIP_VELOCITY

    if (keys_pressed[pygame.K_RIGHT] == True) and (
            spaceship.x + SPACESHIP_VELOCITY + spaceship.width <= SCREEN_WIDTH):
        spaceship.x += SPACESHIP_VELOCITY


def bullets_handler(bullets, enemy):
    for bullet in bullets:
        bullet.y -= BULLET_VELOCITY
        if enemy.colliderect(bullet):
            pygame.event.post(pygame.event.Event(ENEMY_HIT))
            bullets.remove(bullet)
        elif bullet.y < 0:
            bullets.remove(bullet)


def assign_enemy_image(level):
    return ENEMIES[level - 1]['image']


def level_changer(level, enemy_health):
    if enemy_health == 0:
        new_level = level + 1
        new_enemy_health = ENEMIES[new_level - 1]['health']
        new_level_text = MESSAGE_FONT.render("NEXT LEVEL", 1, WHITE)
        SCREEN.blit(new_level_text, [SCREEN_WIDTH // 2 - new_level_text.get_width() // 2, SCREEN_HEIGHT // 2])
        pygame.display.update()
        pygame.time.wait(1500)
        return new_level, new_enemy_health
    else:
        return level, enemy_health


def calculate_score(score, level, enemy_properties):
    hit_points = enemy_properties['velocity'] + level + (level * int(enemy_properties['irregular_moves']))
    return score + hit_points


def main():
    clock = pygame.time.Clock()

    enemy_move_direction = "right"
    bullets = []
    score = 0
    level = 1
    spaceship = pygame.Rect(160, 500, SPACESHIP_IMAGE.get_width(), SPACESHIP_IMAGE.get_height())
    enemy_image = assign_enemy_image(level)
    enemy = pygame.Rect(
        SCREEN_WIDTH // 2 - enemy_image.get_width() // 2, 50, enemy_image.get_width(), enemy_image.get_height()
    )
    enemy_properties = ENEMIES[level - 1]
    enemy_health = enemy_properties['health']

    game_loop = True
    while game_loop:
        clock.tick(FPS)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_loop = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE and len(bullets) < MAX_BULLETS:
                    bullet = pygame.Rect(
                        spaceship.x + spaceship.width // 2 - 3, spaceship.y - 35,
                        BULLET_IMAGE.get_width(), BULLET_IMAGE.get_height()
                    )
                    bullets.append(bullet)

            if event.type == ENEMY_HIT:
                enemy_health -= 1
                score = calculate_score(score, level, enemy_properties)
                level, enemy_health = level_changer(level, enemy_health)
                enemy_image = assign_enemy_image(level)
                enemy = pygame.Rect(
                    SCREEN_WIDTH // 2 - enemy_image.get_width() // 2, 50, enemy_image.get_width(),
                    enemy_image.get_height()
                )

        keys_pressed = pygame.key.get_pressed()
        enemy_move_direction = enemy_movement_handler(enemy_move_direction, enemy, level)
        player_movement_handler(keys_pressed, spaceship)
        bullets_handler(bullets, enemy)

        draw_window(spaceship, enemy, bullets, score, level)

    pygame.quit()


if __name__ == "__main__":
    main()